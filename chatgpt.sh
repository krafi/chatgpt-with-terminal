#!/bin/bash

# Set your OpenAI API key and endpoint
# https://platform.openai.com/account/api-keys (get your api key from this link)
export OPENAI_API_KEY="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"






export OPENAI_ENDPOINT="https://api.openai.com/v1"

RED='\033[0;31m'
GREEN='\033[0;34m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m'

function chat_with_gpt() {
  # Send the prompt to ChatGPT via the OpenAI API
  response=$(curl -s -X POST \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer $OPENAI_API_KEY" \
    -d "{\"model\": \"text-davinci-002\", \"prompt\": \"$1\", \"temperature\": 0.5, \"max_tokens\": 50}" \
    $OPENAI_ENDPOINT/completions)

  echo "$response" | jq -r '.choices[0].text'
}


while true; do
  read -p "You: " input
  if [[ "$input" == "exit" ]]; then
    break
  fi

  # Send the user's input to ChatGPT and print the response
  response=$(chat_with_gpt "$input")
  echo -e "${BLUE}ChatGPT: $response"
done
